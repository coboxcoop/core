# Summary

Please give a detailed summary of the the issue. This is a template for you to use, please try and follow this!

## Step-By-Step

Tell us _exactly_ what you did, step-by-step, before you saw this bug. The more verbose, the better.

## Stack Trace

When reporting an error, post the stack trace. We like it when you put the stack trace inside a code block, like this:

\`\`\`

put the stack trace in here

\`\`\`

## Screenshot 

Screen shots, particularly UI, but also for other issues, are also really helpful!

## Expected

Describe what you had assumed or expected would happen.

## Actual

Describe what actually happened.

