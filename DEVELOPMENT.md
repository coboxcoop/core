# Development

## Acknowledgments
This document has been heavily inspired by our friends at [Ahau](https://gitlab.com/ahau/process/-/raw/master/feature_development.md). 

## Architecture Approach

**CoBox** follows a common P2P pattern of modularity (e.g. DAT projects, secure scuttlebutt). **CoBox** as a software is made up of [many modules](https://develop.cobox.cloud/modules.html).

To ease contribution, core is a mono-repo (parent/umbrella repo) which brings together all the modules. This attempts to make feature development, collaboration and consistent review simpler for all involved. For this We use lerna and git submodules. To see more checkout our [development workflow](./DEVELOPMENT.md).

Generally speaking we always work off the mono-repo, and never off individual packages, as we hope it will reduce issues and make things easier. And it creates a somewhat strict development flow, which we hope takes weight off the mind and puts our development flow into a concrete process.

## Setup

You can run all core processes with the following commands (each in a separate terminal).
```
./cobox client up --dev

./cobox hub up --dev --storage $HOME/.cobox-hub
```

## Building a new Feature

##### 1. To see what needs to be done next:
- Check the issues board, particularly the "Pending" column with "Enchancement" label.
- Pull issues from the "Icebox" and move it into the "Pending" column
##### 2. Move the current issue you are working on into the "Work in Progress" column and assign it to yourself
##### 3. Create a git branch off of `master` for the feature you are working on with a similar name to the issue
##### 4. Create a merge/pull request for the branch you are currently working on, mark it as "Work In Progress" and reference the relevant issue
##### 5. Keep the branch fresh!
- regularly commit changes to the branch.
- regularly pull from the `master` branch, and rebase your feature branch to `master`.
- push all changes up to gitlab, _especially at the end of a working day_
- ensure you write tests and run `npm run cover` to view your test coverage.
##### 6. If you require any feedback before you can complete the issue, use the issue comments describing what you would like feedback on, assign the issue to the person you would like feedback from and let that person know via keybase #development channel or IRC #cobox channel
##### 7. Submit the PR for review
- Remove the "Work In Progress" label
  - Assign the "Testing" label
- Use the description template to add a description of your PR and post the link to your PR in the #development channel for someone to [review](#reviews)
  - If making UI improvements, ensure the designer in included in the issue
##### 8. Tidying up
- after someone has reviewed it, you may need to make some changes and have it reviewed again (loop to 7)
  - rebase your branch and squash badly named commits
##### 9. Merge
  - after everything is good, merge the work, deleting the branch from gitlab
##### 10. Close the issue

### Git Flow

When starting a new feature, we use the following workflow:

```bash
# make sure we're in /path/to/cobox-core
pwd

# starting in `master` branch of `cobox-core`, checkout to a new feature branch `feature/$MY_NEW_FEATURE`
git checkout -b feature/$MY_NEW_FEATURE

# install and link local packages
yarn

# 1. make changes in the first submodule

# change to submodule to make code changes
cd cobox-server

# make sure we're in the child module /path/to/cobox-core/cobox-server
pwd

# checkout to feature branch
git checkout -b feature/$MY_NEW_FEATURE

# ...make code changes and commit...
git add .
git commit -m 'added a new feature'
git push origin feature/$MY_NEW_FEATURE

# return to mono-repo /path/to/cobox-core
cd ..

# update mono-repo to point to latest cobox-server commit
git add cobox-server
git commit -m 'updated commit for cobox-server feature'
git push origin feature/$MY_NEW_FEATURE

# 2. make changes in the second submodule

# change to other submodule to continue feature development
cd cobox-ui

# make sure we're in the child module /path/to/cobox-core/cobox-ui
pwd

# checkout to feature branch
git checkout -b feature/$MY_NEW_FEATURE

# ...make code changes and commit...
git add .
git commit -m 'added a new feature'
git push origin feature/$MY_NEW_FEATURE

# return to mono-repo /path/to/cobox-core
cd ..

# update mono-repo to point to latest cobox-ui commit
git add cobox-ui
git commit -m 'updated commit for cobox-ui feature'
git push origin feature/$MY_NEW_FEATURE

# note how step 1 and step 2 are the same. simply rinse & repeat...

# the important step for other contributors, and reviewers,
# is they want to be able to checkout in cobox-core and it will
# automagically point to the correct submodule branches. they don' need
# to know in advance what the names of the branches are, it just works.

# In order for this to happen, you _must_ update the mono-repository's
# submodule's commit ref.
```

It can be helpful to create a clear checklist of tasks, additions or smaller changes you've made which compose the feature, such as:

```
* [ ] removed old unnecessary comments
* [ ] added controller for invites with create and index actions
* [ ] unit tested invites controller
```

This helps the reviewer with an overview of all the component parts which compose the entire feature, and helps with documenting your own workflow.

## Reviews

There are several sorts of review that can be done. You need to decide which are relevant and name them in communication.

**Please keep in mind** that while you're reviewing another persons work, you're in a position of quite some power over them.
I highly recommend:
- celebrating sweet pieces of code (if you learn a new trick, or love how elegant it is)
  - naming what's working as well as what isn't
  - talking about things which feel good about the change
  - acknowledging the work that's been done if it's big

  All of these things help make a review something you're doing _together_, and reminds you that there are people involved, and that criticism of the code that's
  been written is not a criticism of a person. Good vibes help us all go a lot further together.

### Git Flow

```bash
# make sure we're in /path/to/cobox-core
pwd

# starting in `master` branch of `cobox-core`, checkout to the branch you are reviewing
git checkout -b $REVIEW_BRANCH

# make sure we're pointing to all the correct subcommits
git submodule update

# link local packages, you may need to add `--force-local` option)
yarn
```

If whoever has submitted the pull-request has setup core properly, the reviewer should now be at the same point as them.

### Smoke Test

_If you turn the machine on an push a button, does smoke come out of it?_

In this sort of test, a peer:
- read the description of the piece of work in the PR
- spins up your branch
- click around to see if you can break it
  - try doing silly things (like 100 characters in a name)
  - poke parts of the app adjacent to this feature
  - see if it works if you start from scracth / start with an existing record
  - do the tests pass?
- if you find anything unexpected
  - post detailed notes on the PR
  - take screenshots

It can be helpful to create a clear checklist of things you tried like 

```
- [ ] added a parent to the top of the graph
- [x] added a parent to a person lower in the graph
- [x] added a child
```

### Code Review

The purpose of this sort of review is to ensure the code is safe to merge.
This is measured on several axes:
- **danger** :fire:
  - is this code going to hurt anyone or their data if it's run ?
  - is it going to be incredibly resource intensive
- **technical debt** :wrench:
  - if someone needs to fix / modify this in the future, is it going to be easy to understand?
  - if something _complex_ (e.g. there are many paths through it) was added, is there a test around it?
  - if a beginner copies a pattern they've seen here, is the app going to become a mess?
  - is this adding repetition to the point that it will make maintenance hard (note: copying code 2-3 times can be appropriate)
  - if there's something messy, is it clearly commented as such, with all the info needed to improve the code in the future?
- **style** :sunglasses:
  - is the style consistent with the rest of the app (this sounds strange, but coherent style mean predicatable patterns which ease comprehension and reduce errors)
  - as an application grows, our style will evolve (same if new people join). Let's keep talking about it so that we're writing code that serves us well (i.t.o. comprehension)

_If you don't know what technical debt is yet, it's imperative we talk about it!_

#### Steps for a code review:
- read the description of the piece of work in the PR
- have a high-level look at the files changed
  - any surprises?
  - which files are core to this PR, versus periforal?
- read through the files, this can look like:
  - look for silly mistakes (leftover comments, TODOs, console.logs)
  - follow the flow of logic
  - this can be expensive i.t.o. time, but it can lead to a deeper understanding of the app, which pays off later
  - you don't need to understand everything 100%
  - look for confusing code
  - see if reading it a little longer helps
  - is that function doing too much? 
  - is this the right place for this functionality
  - are there unhelpful variable names like `_v`, `whakapapa`
- leave inline comments
  - clearly mark what sort of comment it is (:fire: = danger!, "style" = let's talk together, this is non-vital. Everything else is just a suggestion)
  - remember to celebrate successes, and leave cute emoji / [kaomoji](http://japaneseemoticons.me/excited-emoticons/)
  - if there's nothing to critique, skip to _merge_
- hand the review to the author
  - direct-message / @-mention the author with a link to the PR
- do follow up review
  - you're on reviewing this branch until it's merged
- merge
  - make sure you check someone has smoke-tested this before you merge. If no-one has, do this yourself now
  - select "delete branch"
  - decide whether to select "squash commits"

#### Making changes as the reviewer

It's best to leave changes in the hands of the author, but sometimes this isn't practical
e.g. it's a linting fixup, it's moving a file without changing the functionality, the person is on holiday

If you really do need to make a change consider:
- do you _really_ need to?
- messing with a persons branch can be really annoying e.g. they might be offline working on it
- is this thing blocking something else? if not, leave it
- making a branch off of this one, and opening a PR to the original branch
- do this _even if you plan to merge it yourself_
- this gives you a place to point to and talk about your changes
- this helps to be able to "show" a person the change you meant without having to do it everywhere. this sort of branch might even be thrown out later
- if it turns into a big change, you may need to take off your reviewer hat, and get someone else to review the final work

### Design / UX / Product Owner review

TODO
