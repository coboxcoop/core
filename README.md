<div align="center">
  <img src="https://cobox.cloud/src/svg/logo.svg">
</div>

# Mono

This is CoBox's developer mono-repo. If you're looking for the main app, check out [CoBox Server](https://gitlab.com/coboxcoop/server). If you're looking for the seeder app, check out [CoBox Seeder](https://gitlab.com/coboxcoop/seeder).

CoBox is made up of many individual modules which stack ontop of each other with slightly different setups to produce two main applications - CoBox Server for the client application, and CoBox Seeder for the blind replicator.

To ease contribution, core is a mono-repo (parent/umbrella repo) which brings together all the modules. This attempts to make feature development, collaboration and consistent review simpler for all involved. For this We use lerna and git submodules. To see more checkout our [development workflow](./DEVELOPMENT.md).

Generally speaking we always work off the mono-repo, and never off individual packages, as we hope it will reduce issues and make things easier. And it creates a somewhat strict development flow, which we hope takes weight off the mind and puts our development flow into a concrete process.

## Getting Started

```
# Clone the repo recursively on either http or ssh
git clone --recursive git@gitlab.com:coboxcoop/core.git

# install dependencies and cross-reference local packages
yarn

# start the app in developer mode
./cobox app up --dev

# start the seeder in developer mode
./cobox seeder up --dev
```

### Development

Check out our [development workflow](./DEVELOPMENT.md).

## About

Coops often rely on proprietary services for managing their data, which do not reflect their cooperative principles or business practices.

CoBox is an innovative suite of open hardware and software providing accessible governance tools for organisations and networks. It comprises a human-centric plug and play server with pre-installed privacy enhancing coop software designed to promote members' data sovereignty. CoBox seeks to build on the historic tendency for networks of coops to collaborate.

CoBox brings a cooperative approach to hosting, treating data as a common good owned by citizens. Leveraging the benefits of self-hosting combined with peer-to-peer technologies to share responsibilities of maintaining data availability.

As well as serving internal organisational governance needs these simple to use tools provide the infrastructure for governance of the CoBox network, to manage, research and guide the ecosystem, providing a state of the art alternative to corporate models.

## Resources

* [MVP TRL 4-5 Submission](https://cobox.cloud/mvp.pdf)
* [Demo Video](https://cobox.cloud/cli-demo-video.webm)
* [Docs](https://develop.cobox.cloud/)
